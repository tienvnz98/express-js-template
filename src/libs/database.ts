'use strict';


import knex, { Knex } from "knex";
import { sleep } from '../utils';
import Event from 'events';
const config = require('../../config.json');

const mySQLHost = (process.env.MY_SQL_HOST || config.MY_SQL_HOST || 'localhost').trim();
const mySQLPort = (process.env.MYSQL_PORT || config.MY_SQL_PORT || '3306').trim();
const mySQLDB = (process.env.MY_SQL_DB || config.MY_SQL_DB || 'abubu').trim();
const mySQLUser = (process.env.MY_SQL_USER || config.MY_SQL_USER || 'admin').trim();
const mySQLPass = (process.env.MY_SQL_PASS || config.MY_SQL_PASS || 'admin').trim();


class DBConn extends Event {
  private static _instance: DBConn;
  private _initialized = false;

  private constructor() {
    super();
  }

  private _retryTime = 5000;
  private _conn: Knex = null;
  private _isReady = false;
  private _lastTimeConnectDB: any = null;

  async init() {
    // Already initialized
    if (this._initialized === true) return;
    this._initialized = true;
    await this._connect();
    const that = this;

    this.on('connection_error', async () => {
      console.log(`Connection ERROR Retry to connect now!`);
      await that._connect();
    });
    this._traceConn();
    this._createLogTable();
  }

  private async _traceConn() {
    const that = this;
    while (true) {
      await (async () => {
        if (that._isReady) {
          const res = await that._ping();
          if (!res) that.emit('connection_error');
        }
      })()
      await sleep(10000);
    }
  }

  private async _connect() {
    this._isReady = false;

    if (this._conn) {
      this._conn.destroy();
      this._conn = null;
    }

    console.log(`\nWait for application connect to Database!\n`);
    this._conn = knex({
      client: 'mysql',
      connection: {
        host: mySQLHost,
        user: mySQLUser,
        password: mySQLPass,
        database: mySQLDB,
        connectTimeout: 1000,
        port: Number(mySQLPort)
      },
      pool: {
        min: 3,
        max: 7
      },
      migrations: {
        tableName: 'mig'
      },
      acquireConnectionTimeout: 1000
    });

    while (!this._isReady) {
      this._isReady = await this._ping();
      if (!this._isReady) {
        console.log(`${new Date()}:[Error] Connect to ${mySQLHost}:${mySQLPort} DB Fail. Retry connect after ${this._retryTime} ms.`);
        await sleep(this._retryTime);
      } else {
        console.log(`DB connect success ${mySQLHost} ${mySQLPort}`);
        this._lastTimeConnectDB = new Date().toISOString();
      }
    }
  }

  private async _ping() {
    if (!this._conn) return false;
    return await this._conn.raw('SELECT 5+5 as TEST')
      .then(res => (res && res[0] && res[0][0].TEST === 10)
      ).catch(err => false);
  }

  private async _createLogTable(): Promise<any> {
    if (!this._isReady) return;

    const now = new Date();
    const tableName = `TRAN_LOGS_${now.getMonth() + 1}_${now.getFullYear()}`;
    const isExists = await this._conn.schema.hasTable(tableName).catch(err => false);

    return isExists ? true : await this._conn.schema.createTable(tableName, (table) => {
      table.increments()
      table.string('table_name').notNullable();
      table.string('operation').notNullable();
      table.string('action_type').notNullable();
      table.json('old_data').notNullable();
      table.json('new_data').notNullable();
      table.timestamp('created_at').notNullable();
      return table;
    }).catch(err => {
      console.log(err.message);
    });
  }

  public async _log(table: string, operation: string, actionType: string, oldData: any, newData: any): Promise<any> {
    const now = new Date();
    const tableName = `TRAN_LOGS_${now.getMonth() + 1}_${now.getFullYear()}`;
    const that = this;

    let res = false;
    let retry = 0;

    while (!res && retry++ < 100) {
      res = await this._conn(tableName).insert({
        table_name: table,
        operation: operation,
        action_type: actionType,
        old_data: JSON.stringify(oldData),
        new_data: JSON.stringify(newData),
      }).then(res => !!res).catch(async (err) => {
        await that._createLogTable();
        return false;
      });

      if (!res) {
        console.log(`[ERROR]: Log data fail.`);
        await sleep(5000);
      }
    }
  }

  public async insertOne(table: string, data: any): Promise<any> {
    const result = {
      success: false,
      id: 0,
      error: ''
    }

    const res = await this._conn(table).insert({
      ...data
    }).then(res => {
      result.success = true;
      result.id = res[0];
    }).catch(err => {
      result.error = err.message;
    });

    if (result.success) {
      this._log(table, 'C', 'I', {}, data);
    }

    return result;
  }

  public async getOne(table: string, ID: number): Promise<any> {
    if (!this._isReady) {
      this._traceConn();
      return null;
    }

    const item = await this._conn
      .select('*')
      .limit(1)
      .from(table)
      .where('ID', ID)
      .then(res => {
        if (!res) res = [];
        return {
          error: false,
          message: '',
          data: res[0] || null
        };
      }).catch(err => {
        return {
          error: true,
          message: err.message,
          data: null
        }
      });

    return item;
  }

  public async updateOne(table: string, ID: number, data: any): Promise<any> {
    if (!this._isReady) {
      this._traceConn();
      return null;
    }

    const old = await this.getOne(table, ID);

    if (!old.error && old.data) {
      const oldData: any = {};
      const newData: any = {};
      const fields = Object.keys(data);
      let haveChange = false;

      for (const field of fields) {
        if (old.data[field] !== data[field]) {
          haveChange = true;
          oldData[field] = old.data[field];
          newData[field] = data[field];
        }
      }

      let result = null;

      if (haveChange) {
        result = await this._conn(table)
          .where('ID', ID)
          .update({ ...data })
          .then(res => {
            return {
              error: false,
              result: res
            };
          }).catch(err => {
            return {
              error: true,
              message: err.message
            };
          });

        if (result) {
          this._log(table, 'U', 'C', oldData, newData);
        }
      }

      return result;
    }

    return null;
  }

  public async deleteOne(table: string, ID: Number): Promise<any> {
    if (!this._isReady) {
      this._traceConn();
      return null;
    }

    const result = await this._conn(table)
      .where('ID', ID)
      .del()
      .then(res => {
        return {
          error: false,
          result: res
        };
      }).catch(err => {
        return {
          error: true,
          message: err.message
        };
      });

    return result;
  }

  public async rawList(queryString: string) {

  }

  public async rawUpdate(queryString: string) {

  }

  public async rawDelete(queryString: string) {

  }

  public get conn(): Knex {
    return this._conn;
  }

  public get isReady(): any {
    return {
      ready: !!this._isReady,
      lastConnect: this._lastTimeConnectDB
    };
  }

  static get Instance(): DBConn {
    return this._instance || (this._instance = new this());
  }
}

export default DBConn.Instance;