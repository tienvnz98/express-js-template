'use strict';


import { Request, Response, NextFunction } from '../interfaces/express';
import DBConn from '../libs/database';
const pkg = require('../../package.json');

export async function homeHandle(req: Request, res: Response, next: NextFunction) {
  try {
    // Handlers here
    return res.showResult({
      info: 'Abubu API Services',
      version: pkg.version,
      dbReady: DBConn.isReady
    });
  } catch (error) {
    return next(error);
  }
}