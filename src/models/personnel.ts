'use strict';

import Event from 'events';
import DBConn from '../libs/database';


class PersonnelModel extends Event {
  private static _instance: PersonnelModel;
  private _initialized = false;
  private _tableName = 'PERSONNEL';

  private async _createTableIfNotExist() {
    const isExists = await DBConn.conn.schema.hasTable(this._tableName).catch(err => false);
    return isExists ? true : await DBConn.conn.schema.createTable(this._tableName, (table) => {
      table.increments();

      table.integer('store_id').notNullable();
      table.integer('user_id').notNullable();
      table.unique(['store_id', 'user_id']);
      
      return true;
    }).then(res => {
      console.log(`Create table ${this._tableName} success!`);
    }).catch(err => {
      console.log(err.message);
      return false;
    });
  }

  private constructor() {
    super();
  }

  async init() {
    // Already initialized
    if (this._initialized === true) return;
    this._initialized = true;
    await this._createTableIfNotExist();
  }

  static get Instance(): PersonnelModel {
    return this._instance || (this._instance = new this());
  }
}

export default PersonnelModel.Instance;