'use strict';

import dotenv from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { router } from './routes';
import { middleFunction } from './middlewares/middle-functions';
import { authentication } from './middlewares/authenticaton';
import { notFoundResource } from './middlewares/not-found';
import { errorHandler } from './middlewares/error-handlers';
import { initModel } from './models';
import DBConn from './libs/database';

if (process.env.NODE_ENV !== 'production') {
  dotenv.config();
}

const port = process.env.PORT || 8300;
const app = express();

process.on('uncaughtException', function (err) {
  console.log(err.message);
});

app
  .use(cors())
  .use(async (error: any, req: any, res: any, next: any) => {
    try {
      next()
    } catch (error) {
      console.log(error.message);
    }
  })
  .use(bodyParser())
  .use(middleFunction())
  .use(authentication())
  .use(router)
  .use(errorHandler())
  .use(notFoundResource())

async function start() {
  DBConn.init();
  // await initModel();
  app.listen(port, () => {
    console.log(`${new Date} The application is listening on port ${port}!`);
  });
}

start();
