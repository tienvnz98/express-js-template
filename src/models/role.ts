'use strict';

import Event from 'events';
import DBConn from '../libs/database';


class RoleModel extends Event {
  private static _instance: RoleModel;
  private _initialized = false;
  private _tableName = 'ROLE';

  private async _createTableIfNotExist() {
    const isExists = await DBConn.conn.schema.hasTable(this._tableName).catch(err => false);
    return isExists ? true : await DBConn.conn.schema.createTable(this._tableName, (table) => {
      table.increments();
      table.integer('store_id').notNullable();
      table.string('name').notNullable();
      table.integer('user_id').notNullable();
      table.boolean('status').notNullable().defaultTo(true);
      table.unique(['name', 'store_id', 'user_id']);
      return true;
    }).then(res => {
      console.log(`Create table ${this._tableName} success!`);
    }).catch(err => {
      console.log(err.message);
      return false;
    });
  }

  private constructor() {
    super();
  }

  async init() {
    // Already initialized
    if (this._initialized === true) return;
    this._initialized = true;
    await this._createTableIfNotExist();
  }

  static get Instance(): RoleModel {
    return this._instance || (this._instance = new this());
  }
}

export default RoleModel.Instance;