'use strict';

import Event from 'events';
import DBConn from '../libs/database';


class StoreModel extends Event {
  private static _instance: StoreModel;
  private _initialized = false;
  private _tableName = 'STORE';

  private async _createTableIfNotExist() {
    const isExists = await DBConn.conn.schema.hasTable(this._tableName).catch(err => false);
    return isExists ? true : await DBConn.conn.schema.createTable(this._tableName, (table) => {

      table.increments();
      table.string('owner').notNullable();
      table.string('name').notNullable();
      table.string('address').notNullable();
      table.string('phone_number').notNullable().unique();
      table.string('email').notNullable().unique();
      table.string('avatar').notNullable();
      table.integer('root_cost_type').notNullable();
      table.integer('sell_cost').notNullable();
      table.unique(['owner', 'name']);
      
      return true;
    }).then(res => {
      console.log(`Create table ${this._tableName} success!`);
    }).catch(err => {
      console.log(err.message);
      return false;
    });
  }

  private constructor() {
    super();
  }

  async init() {
    // Already initialized
    if (this._initialized === true) return;
    this._initialized = true;
    await this._createTableIfNotExist();
  }

  static get Instance(): StoreModel {
    return this._instance || (this._instance = new this());
  }
}

export default StoreModel.Instance;