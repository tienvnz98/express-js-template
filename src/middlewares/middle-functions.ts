import { Request, Response, NextFunction } from '../interfaces/express';


export function middleFunction() {
  return async (req: Request, res: Response, next: NextFunction) => {
    res.showError = (messages: any, status?: number) => {
      const body = {
        success: false,
        messages: messages
      }

      res.status(status || 400)
        .send(body)
        .end();
    }

    res.showResult = (data: any, status?: number) => {
      const body = {
        success: true,
        data: data
      }

      res.status(status || 200)
        .send(body)
        .end();
    }

    next();
  }
}