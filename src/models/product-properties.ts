'use strict';

import Event from 'events';
import DBConn from '../libs/database';


class ProductPropertyModel extends Event {
  private static _instance: ProductPropertyModel;
  private _initialized = false;
  private _tableName = 'PRODUCT_PROPERTIES';

  private async _createTableIfNotExist() {
    const isExists = await DBConn.conn.schema.hasTable(this._tableName).catch(err => false);

    return isExists ? true : await DBConn.conn.schema.createTable(this._tableName, (table) => {
      table.increments();
      table.integer('product_id').notNullable();
      table.string('name').notNullable();
      table.string('attribute').notNullable();
      table.integer('variable').notNullable();
      table.string('uom').notNullable();
      return true;
    }).then(res => {
      console.log(`Create table ${this._tableName} success!`);
    }).catch(err => {
      console.log(err.message);
      return false;
    });
  }

  private constructor() {
    super();
  }

  async init() {
    // Already initialized
    if (this._initialized === true) return;
    this._initialized = true;
    await this._createTableIfNotExist();
  }

  static get Instance(): ProductPropertyModel {
    return this._instance || (this._instance = new this());
  }
}

export default ProductPropertyModel.Instance;