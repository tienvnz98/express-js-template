import Joi from "joi";

const homeSchema = Joi.object({
    yourName: Joi.string().required(),
    yourBirthYear: Joi.number().min(1950).max(2020).required()
});

export {
    homeSchema
}