export async function sleep(time: number) {
    await new Promise((solve) => {
        setTimeout(() => {
            solve(true)
        }, time);
    })
}