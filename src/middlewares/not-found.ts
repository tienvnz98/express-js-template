'use strict';
import { Request, Response, NextFunction } from '../interfaces/express';

export function notFoundResource() {
    return async (req: Request, res: Response, next: NextFunction) => {
        const method = req.method;
        const url = req.url;
        return res.showError(`Not found resource ${method} ${url}`, 404);
    }
}