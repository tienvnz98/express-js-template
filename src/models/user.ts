'use strict';

import Event from 'events';
import DBConn from '../libs/database';


class UserModel extends Event {
  private static _instance: UserModel;
  private _initialized = false;
  private _tableName = 'USER';

  private async _createTableIfNotExist() {
    const isExists = await DBConn.conn.schema.hasTable(this._tableName).catch(err => false);
    return isExists ? true : await DBConn.conn.schema.createTable(this._tableName, (table) => {
      table.increments();
      table.string('username').unique().notNullable();
      table.string('password').notNullable();
      table.string('OTP').nullable();
      table.string('email').nullable();
      table.string('phone_number').nullable();
      table.string('full_name').notNullable();
      table.string('avatar').nullable();
      table.date('dob').nullable();
      table.string('created_by').notNullable();
      table.timestamp('created_date');
      table.integer('invalid_pass_count').defaultTo(0);
      table.string('firebase_token').notNullable();
      return true;
    }).then(res => {
      console.log(`Create table ${this._tableName} success!`);
    }).catch(err => {
      return false;
    });
  }

  private constructor() {
    super();
  }

  async init() {
    // Already initialized
    if (this._initialized === true) return;
    this._initialized = true;
    await this._createTableIfNotExist();
  }

  static get Instance(): UserModel {
    return this._instance || (this._instance = new this());
  }
}

export default UserModel.Instance;