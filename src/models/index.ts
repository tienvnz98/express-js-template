'use strict';


import User from './user';
import Role from './role';
import Permissions from './permissions'
import SiginHistory from './sigin-history';
import CashBook from './cash-book';
import Partner from './partner';
import Guest from './guest';
import ProductGroup from './product-group';
import Brand from './brand';
import ProductProperty from './product-properties';
import Product from './product';
import Warehouse from './warehouse';
import WarehouseDetails from './warehouse-details';
import Personnel from './personnel';

export async function initModel() {
  await Promise.all([
    User.init(),
    Role.init(),
    Personnel.init(),
    Permissions.init(),
    SiginHistory.init(),
    CashBook.init(),
    Partner.init(),
    Guest.init(),
    ProductGroup.init(),
    Brand.init(),
    ProductProperty.init(),
    Product.init(),
    Warehouse.init(),
    WarehouseDetails.init()
  ]);

}