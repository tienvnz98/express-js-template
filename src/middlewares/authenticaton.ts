import { Request, Response, NextFunction } from '../interfaces/express';
import jwt from 'jsonwebtoken';

const secretKey = process.env.JWT_SECRET || '&*@#$HGSED_@#+MGHHdsKK@#$&)hdfhjskagf';


export function authentication() {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      const paths = req.url.split('/');
      const pathPrefix = paths[1];

      if (!pathPrefix?.endsWith('public') && req.url !== '/') {
        const token = req.headers?.authorization?.replace('Bearer', '').trim() || '';
        if (!token) return res.showError('Request token required!', 401);


        let msg = '';

        try {
          //jwt.verify(token, secretKey);
        } catch (error) {
          msg = error.message;
        }

        if (msg) {
          return res.showError(msg, 401);
        }

        const payloadInfo = jwt.decode(token);
        req.token = payloadInfo;
      }
      next();
    } catch (error) {
      next(error);
    }
  }
}