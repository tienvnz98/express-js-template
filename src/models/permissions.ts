'use strict';

import Event from 'events';
import DBConn from '../libs/database';


class PermissionModel extends Event {
  private static _instance: PermissionModel;
  private _initialized = false;
  private _tableName = 'PERMISSIONS';

  private async _createTableIfNotExist() {
    const isExists = await DBConn.conn.schema.hasTable(this._tableName).catch(err => false);
    return isExists ? true : await DBConn.conn.schema.createTable(this._tableName, (table) => {
      table.increments();
      table.integer('store_id').defaultTo('default_store').notNullable();
      table.integer('role_id').notNullable();
      table.string('name').notNullable();
      table.unique(['store_id', 'role_id', 'name']);
      return true;
    }).then(res => {
      console.log(`Create table ${this._tableName} success!`);
    }).catch(err => {
      console.log(err.message);
      return false;
    });
  }

  private constructor() {
    super();
  }

  async init() {
    // Already initialized
    if (this._initialized === true) return;
    this._initialized = true;
    await this._createTableIfNotExist();
  }

  static get Instance(): PermissionModel {
    return this._instance || (this._instance = new this());
  }
}

export default PermissionModel.Instance;