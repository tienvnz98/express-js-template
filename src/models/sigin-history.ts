'use strict';

import Event from 'events';
import DBConn from '../libs/database';


class SiginHistoryModel extends Event {
  private static _instance: SiginHistoryModel;
  private _initialized = false;
  private _tableName = 'SIGIN_HISTORY';

  private async _createTableIfNotExist() {
    const isExists = await DBConn.conn.schema.hasTable(this._tableName).catch(err => false);
    return isExists ? true : await DBConn.conn.schema.createTable(this._tableName, (table) => {
      table.increments();
      table.string('user_id').notNullable();
      table.string('platform').notNullable().defaultTo('unknown');
      table.string('location').notNullable().defaultTo('unknown');
      table.string('ip_address').defaultTo('unknown');
      table.timestamp('created_date').notNullable();
      table.boolean('success').defaultTo(false);
      return true;
    }).then(res => {
      console.log(`Create table ${this._tableName} success!`);
    }).catch(err => {
      console.log(err.message);
      return false;
    });
  }

  private constructor() {
    super();
  }

  async init() {
    // Already initialized
    if (this._initialized === true) return;
    this._initialized = true;
    await this._createTableIfNotExist();
  }

  static get Instance(): SiginHistoryModel {
    return this._instance || (this._instance = new this());
  }
}

export default SiginHistoryModel.Instance;