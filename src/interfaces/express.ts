import I from 'express';

interface showError {
    (messages: any, status?: number): void
}

interface showResult {
    (data: any, status?: number): void
}

export interface Request extends I.Request {
    // Add more attribute here
    token: any
}
export interface Response extends I.Response {
    // Add more attribute here
    showResult: showResult,
    showError: showError
}

export interface NextFunction extends I.NextFunction {
    // Add more attribute here
}

export interface ErrorRequestHandler extends I.ErrorRequestHandler {

}