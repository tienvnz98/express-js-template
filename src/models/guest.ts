'use strict';

import Event from 'events';
import DBConn from '../libs/database';


class GuestModel extends Event {
  private static _instance: GuestModel;
  private _initialized = false;
  private _tableName = 'GUEST';

  private async _createTableIfNotExist() {
    const isExists = await DBConn.conn.schema.hasTable(this._tableName).catch(err => false);
    return isExists ? true : await DBConn.conn.schema.createTable(this._tableName, (table) => {
      table.increments();

      table.integer('store_id').notNullable();
      table.bigInteger('credit_limit').notNullable().defaultTo(0);
      table.integer('cycle_credit').notNullable().defaultTo(0);
      table.string('name').notNullable().defaultTo('unknown');
      table.string('phone_number').notNullable().defaultTo('unknown');

      return true;
    }).then(res => {
      console.log(`Create table ${this._tableName} success!`);
    }).catch(err => {
      console.log(err.message);
      return false;
    });
  }

  private constructor() {
    super();
  }

  async init() {
    // Already initialized
    if (this._initialized === true) return;
    this._initialized = true;
    await this._createTableIfNotExist();
  }

  static get Instance(): GuestModel {
    return this._instance || (this._instance = new this());
  }
}

export default GuestModel.Instance;