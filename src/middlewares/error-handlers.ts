import { Request, Response, NextFunction } from '../interfaces/express';


export function errorHandler() {
  return async (err: Error, req: Request, res: Response, next: NextFunction) => {
    if (err) {
      return res.showError(err.message, 500);
    }
    next();
  }
}