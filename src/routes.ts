'use strict';


import { Router, RouterOptions } from 'express';
import * as home from './controllers/home';

const router: any = Router();

router.get('/', home.homeHandle);


export { router }